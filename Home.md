# 1. Datenschutz

Tiny Weather Forecast Germany ruft Wetterdaten und Wetter-Warnungen aus dem Open-Data-Portal des [Deutschen Wetterdienstes](https://www.dwd.de/DE/Home/home_node.html) ab.

Dazu ist es technisch notwendig, im Rahmen der Internetverbindung die IP-Adresse an den Deutschen Wetterdienst zu übermitteln und den Ort/Sensor, dessen Daten abgerufen werden sollen. Bei den Warnungen wird auch übermittelt, ob diese (abhängig von der Spracheinstellung des Gerätes) auf Französisch, Spanisch, Deutsch oder Englisch abgerufen werden sollen. Wie der Deutsche Wetterdienst mit diesen Daten umgeht und wie er sie speichert kann in der [Datenschutzerklärung des Deutschen Wetterdienstes](https://www.dwd.de/DE/service/datenschutz/datenschutz_node.html) nachgelesen werden.

Die abgerufenen Wettervorhersagen und Warnungen werden **lokal** auf dem Gerät gespeichert, ebenso die eingestellten Orte/Sensoren. Die Einstellungen und wenige interne Variablen wie z.B. wann zuletzt eine Wetterwarnung abgerufen wurde werden ebenfalls auf dem Gerät lokal gespeichert.

Sofern vom Benutzer aktiviert, gibt Tiny Weather Forecast Germany die Wetterdaten lokal auf dem Gerät an die App [Gadgetbridge](https://codeberg.org/Freeyourgadget/Gadgetbridge) weiter, damit die Wetterdaten auch auf Wearables angezeigt werden können.

Wetter-Warnungen können durch Benutzer-Interaktion geteilt werden.

Darüber hinaus speichert, versendet und teilt Tiny Weather Forecast Germany keine Daten.

# 1. Data Protection / Privacy

Tiny Weather Forecast Germany polls weather data and weather warnings from the open-data portal of the [Deutscher Wetterdienst](https://www.dwd.de/DE/Home/home_node.html).

Do do so, it is technically required to send your IP-address to the Deutscher Wetterdienst in the context of the internet connection and also the place/sensor, for which the data has to be polled. In case of the warnings it is also transmitted if they should be polled in French, Spanish, German or English, depending on the language setting of the device.  How this data is handled and stored by the Deutscher Wetterdienst is explained in the [privacy policy of the Deutscher Wetterdienst](https://www.dwd.de/EN/service/dataprotection/dataprotection_node.html).

The weather forecasts and weather warnings are saved **locally** on the device, also the set-up places/sensors. The settings and some internal variables like the last time of a weather warnings poll are also saved locally on the device.

If activated by the user, Tiny Weather Forecast Germany passes the weather data locally on the device to the [Gadgetbridge](https://codeberg.org/Freeyourgadget/Gadgetbridge) app, so that the weather data can be displayed on wearables.

Weather alerts can be shared through user interaction.

Beyond that, Tiny Weather Forecast Germany does not save, send and/or share any data.
